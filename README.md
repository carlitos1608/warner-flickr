## This is a mobile code test for WarnerMedia
* [Setup Developer Key](#developer-key)

## Developer Key

I added developer key as Hidden in BuildConfig. Created locally an "apikey.properties" file which was included in .gitignore that way it does not goes to the remote repo.

"apikey.properties" file only contain the following:
API_KEY="XXXX"

Key is going to be generated at compile time in gradle as following:

    def apikeyPropertiesFile = rootProject.file("apikey.properties")
    def apikeyProperties = new Properties()
    apikeyProperties.load(new FileInputStream(apikeyPropertiesFile))
    
    android {
        defaultConfig {
            buildConfigField("String", "API_KEY", apikeyProperties['API_KEY'])
        }
    }

Key can be retrieved back in the code as following:

    val apiKey = BuildConfig.API_KEY