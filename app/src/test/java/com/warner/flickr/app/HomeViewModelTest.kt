package com.warner.flickr.app

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.warner.flickr.app.repository.HomeRepository
import com.warner.flickr.app.repository.model.PhotoEntity
import com.warner.flickr.app.repository.model.SearchDao
import com.warner.flickr.app.repository.model.SearchEntity
import com.warner.flickr.app.repository.service.HomeWebService
import com.warner.flickr.app.ui.home.HomeUiModel
import com.warner.flickr.app.ui.home.PhotoUiModel
import com.warner.flickr.app.utilities.UrlUtils
import com.warner.flickr.app.viewModel.HomeViewModel
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verifySequence
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Test

import org.junit.Before
import org.junit.Rule
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class HomeViewModelTest {

    @get:Rule
    val textInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    lateinit var searchDao: SearchDao

    @Mock
    lateinit var homeWebService: HomeWebService

    lateinit var homeRepository: HomeRepository

    lateinit var homeViewModel: HomeViewModel

    private val observer = mockk<Observer<HomeViewModel.HomeUiState>>()

    private val slot = slot<HomeViewModel.HomeUiState>()

    private val list = arrayListOf<HomeViewModel.HomeUiState>()

    @Before
    fun setup() {
        homeRepository = HomeRepository(searchDao, homeWebService)
        homeViewModel = HomeViewModel(homeRepository)
    }

    @Test
    fun test_service_success() {
        testCoroutineRule.runBlockingTest {

            // Given
            Mockito.`when`(searchDao.selectSearchByValue(Mockito.anyString())).thenReturn(null)
            Mockito.`when`(homeWebService.searchPhotos(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt(), Mockito.anyString(), Mockito.anyInt()))
                .thenReturn(buildPhotosResponse())

            homeViewModel.homeUiState.observeForever(observer)

            every { observer.onChanged(capture(slot)) } answers {
                list.add(slot.captured)
            }

            // When
            homeViewModel.searchPhotos(DEFAULT_SEARCH)

            // Then
            verifySequence {
                observer.onChanged(HomeViewModel.HomeUiState(state = HomeViewModel.HomeUiState.LOADING))
                observer.onChanged(HomeViewModel.HomeUiState(state = HomeViewModel.HomeUiState.SUCCESS, homeUiModel = HomeUiModel(buildPhotoUiModelList())))
            }
        }
    }

    @Test
    fun test_service_error() {
        testCoroutineRule.runBlockingTest {

            // Given
            Mockito.`when`(searchDao.selectSearchByValue(Mockito.anyString())).thenReturn(null)
            Mockito.`when`(homeWebService.searchPhotos(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt(), Mockito.anyString(), Mockito.anyInt()))
                .thenReturn(null)

            homeViewModel.homeUiState.observeForever(observer)

            every { observer.onChanged(capture(slot)) } answers {
                list.add(slot.captured)
            }

            // When
            homeViewModel.searchPhotos(DEFAULT_SEARCH)


            // Then
            verifySequence {
                observer.onChanged(HomeViewModel.HomeUiState(state = HomeViewModel.HomeUiState.LOADING))
                observer.onChanged(HomeViewModel.HomeUiState(state = HomeViewModel.HomeUiState.ERROR, errorMessage = "Error fetching from Cloud; Null response retrieved"))
            }
        }
    }


    @Test
    fun test_database_success() {
        testCoroutineRule.runBlockingTest {
            // Given
            Mockito.`when`(searchDao.selectSearchByValue(Mockito.anyString())).thenReturn(SearchEntity(0, DEFAULT_SEARCH))
            Mockito.`when`(searchDao.selectPhotosBySearchId(Mockito.anyInt())).thenReturn(buildPhotoEntityList())

            homeViewModel.homeUiState.observeForever(observer)

            every { observer.onChanged(capture(slot)) } answers {
                list.add(slot.captured)
            }

            // When
            homeViewModel.searchPhotos(DEFAULT_SEARCH)

            // Then
            verifySequence {
                observer.onChanged(HomeViewModel.HomeUiState(state = HomeViewModel.HomeUiState.LOADING))
                observer.onChanged(HomeViewModel.HomeUiState(state = HomeViewModel.HomeUiState.SUCCESS, homeUiModel = HomeUiModel(buildPhotoUiModelList())))
            }
        }
    }

    @Test
    fun test_database_error_should_fallback_on_response() {
        testCoroutineRule.runBlockingTest {

            // Given
            Mockito.`when`(searchDao.selectSearchByValue(Mockito.anyString())).thenReturn(SearchEntity(0, DEFAULT_SEARCH))
            Mockito.`when`(searchDao.selectPhotosBySearchId(Mockito.anyInt())).thenReturn(emptyList()) // <- retrieving back an empty list

            // Web Service response fallback
            Mockito.`when`(homeWebService.searchPhotos(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt(), Mockito.anyString(), Mockito.anyInt()))
                .thenReturn(buildPhotosResponse())

            homeViewModel.homeUiState.observeForever(observer)

            every { observer.onChanged(capture(slot)) } answers {
                list.add(slot.captured)
            }

            // When
            homeViewModel.searchPhotos(DEFAULT_SEARCH)

            // Then
            verifySequence {
                observer.onChanged(HomeViewModel.HomeUiState(state = HomeViewModel.HomeUiState.LOADING))
                observer.onChanged(HomeViewModel.HomeUiState(state = HomeViewModel.HomeUiState.SUCCESS, homeUiModel = HomeUiModel(buildPhotoUiModelList())))
            }
        }
    }

    companion object {
        const val DEFAULT_SEARCH = "car"

        fun buildPhotosResponse() =
            HomeWebService.PhotosResponse(
                HomeWebService.Photos(
                    0,
                    listOf(
                        HomeWebService.Photo("", "", "", "", ""),
                        HomeWebService.Photo("", "", "", "", ""),
                        HomeWebService.Photo("", "", "", "", "")
                    )
                ),
                ""
            )

        fun buildPhotoEntityList() =
            listOf(
                PhotoEntity("", 0, "", UrlUtils.toThumbnailUrl("","",""), UrlUtils.toImageUrl("","","")),
                PhotoEntity("", 0, "", UrlUtils.toThumbnailUrl("","",""), UrlUtils.toImageUrl("","","")),
                PhotoEntity("", 0, "", UrlUtils.toThumbnailUrl("","",""), UrlUtils.toImageUrl("","",""))
            )

        fun buildPhotoUiModelList() =
            listOf(
                PhotoUiModel("", "", UrlUtils.toThumbnailUrl("","",""), UrlUtils.toImageUrl("","","")),
                PhotoUiModel("", "", UrlUtils.toThumbnailUrl("","",""), UrlUtils.toImageUrl("","","")),
                PhotoUiModel("", "", UrlUtils.toThumbnailUrl("","",""), UrlUtils.toImageUrl("","",""))
            )
    }
}