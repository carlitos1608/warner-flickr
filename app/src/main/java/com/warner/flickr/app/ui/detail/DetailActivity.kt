package com.warner.flickr.app.ui.detail

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.squareup.picasso.Picasso
import com.warner.flickr.app.R
import com.warner.flickr.app.databinding.ActivityDetailBinding

class DetailActivity : AppCompatActivity() {

    lateinit var binding: ActivityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDetailBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val imageUrl = intent.getStringExtra(IMG_URL)
        imageUrl?.let { url ->
            Picasso
                .with(this)
                .load(url)
                .placeholder(R.mipmap.ic_launcher)
                .into(binding.imgPhoto)
        }

        binding.txtClose.setOnClickListener {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    companion object {
        const val IMG_URL = "img_url"

        fun newIntent(context: Context, imageUrl: String) =
            Intent(context, DetailActivity::class.java).apply {
                putExtra(IMG_URL, imageUrl)
            }

    }
}