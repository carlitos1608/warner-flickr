package com.warner.flickr.app.repository.model

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [SearchEntity::class, PhotoEntity::class], version = 1)
abstract class SearchDatabase : RoomDatabase() {
    abstract fun searchDao(): SearchDao

    companion object {
        const val DATA_BASE_NAME = "search_data_base"
    }
}