package com.warner.flickr.app.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.warner.flickr.app.repository.service.HomeWebService
import com.warner.flickr.app.utilities.UrlUtils

@Entity
data class SearchEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val value: String
) {
    companion object {

        fun fromResponse(searchValue: String): SearchEntity {
            return SearchEntity(value = searchValue)
        }
    }
}

@Entity
data class PhotoEntity(
    @PrimaryKey
    val id: String,
    val searchId: Int,
    val title: String,
    val thumb: String,
    val image: String
) {
    companion object {

        fun fromResponse(searchId: Long, photosResponse: HomeWebService.PhotosResponse): List<PhotoEntity> {
            val listOfPhotos = arrayListOf<PhotoEntity>()
            photosResponse.photos.photo.forEach { photoResponse ->

                val thumbnailUrl =
                    UrlUtils.toThumbnailUrl(
                        photoResponse.server, photoResponse.id, photoResponse.secret
                    )

                val imageUrl =
                    UrlUtils.toImageUrl(
                        photoResponse.server, photoResponse.id, photoResponse.secret
                    )

                listOfPhotos.add(
                    PhotoEntity(
                        id = photoResponse.id,
                        searchId = searchId.toInt(),
                        title = photoResponse.title,
                        thumb = thumbnailUrl,
                        image = imageUrl
                    )
                )
            }

            return listOfPhotos
        }
    }
}