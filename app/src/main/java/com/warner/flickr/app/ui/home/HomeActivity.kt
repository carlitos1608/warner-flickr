package com.warner.flickr.app.ui.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.warner.flickr.app.R
import com.warner.flickr.app.databinding.ActivityHomeBinding
import com.warner.flickr.app.ui.detail.DetailActivity
import com.warner.flickr.app.viewModel.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint(AppCompatActivity::class)
class HomeActivity : Hilt_HomeActivity() {

    private lateinit var binding: ActivityHomeBinding
    private lateinit var homeAdapter: HomeAdapter

    private val homeViewModel: HomeViewModel by viewModels(
        factoryProducer = { defaultViewModelProviderFactory }
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        homeAdapter = HomeAdapter(object : OnClickListener {
            override fun onClicked(imageUrl: String) {
                startActivity(DetailActivity.newIntent(this@HomeActivity, imageUrl))
            }
        })

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(this@HomeActivity)
            adapter = homeAdapter
        }

        binding.btnSearch.setOnClickListener {
            search()
        }

        homeViewModel.homeUiState.observe(this){ uiState ->
            when (uiState.state) {
                HomeViewModel.HomeUiState.LOADING -> onLoading()

                HomeViewModel.HomeUiState.SUCCESS -> onSuccess(uiState)

                HomeViewModel.HomeUiState.ERROR -> onError(uiState)

            }
        }
    }

    private fun search() {
        val searchable = binding.editSearch.text.toString()
        homeViewModel.searchPhotos(searchable)
    }

    private fun enableSearch(enable: Boolean) {
        binding.editSearch.isEnabled = enable
        binding.btnSearch.isEnabled = enable
    }

    private fun onLoading() {
        enableSearch(false)

        binding.txtUtilityText.visibility = View.GONE
        binding.recyclerView.visibility = View.GONE
        binding.progressLoader.visibility = View.VISIBLE
    }

    private fun onSuccess(uiState: HomeViewModel.HomeUiState) {
        enableSearch(true)

        val homeUiModel = uiState.homeUiModel ?: return

        binding.txtUtilityText.visibility = View.GONE
        binding.progressLoader.visibility = View.GONE
        binding.recyclerView.visibility = View.VISIBLE

        homeAdapter.setData(homeUiModel.photos)
    }

    private fun onError(uiState: HomeViewModel.HomeUiState) {
        enableSearch(true)

        binding.progressLoader.visibility = View.GONE
        binding.recyclerView.visibility = View.GONE

        with(binding.txtUtilityText) {
            visibility = View.VISIBLE
            setTextColor(getColor(R.color.red))
            text = uiState.errorMessage
        }
    }
}