package com.warner.flickr.app.utilities

object UrlUtils {

    fun toThumbnailUrl(serverId: String, photoId: String, secret: String) =
        getBaseUrl(serverId, photoId, secret)
            .replace(Constants.FORMAT_SUFFIX, "t")

    fun toImageUrl(serverId: String, photoId: String, secret: String) =
        getBaseUrl(serverId, photoId, secret)
            .replace(Constants.FORMAT_SUFFIX, "w")

    private fun getBaseUrl(serverId: String, photoId: String, secret: String) =
        Constants.BASE_IMG_URL
            .replace(Constants.SERVER_ID_SUFFIX, serverId)
            .replace(Constants.ID_SUFFIX, photoId)
            .replace(Constants.SECRET_SUFFIX, secret)
}