package com.warner.flickr.app.repository.model

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Transaction

@Dao
abstract class SearchDao {

    @Insert(onConflict = REPLACE)
    abstract fun insertSearch(searchEntity: SearchEntity): Long

    @Insert(onConflict = REPLACE)
    abstract fun insertAll(photos: List<PhotoEntity>)

    @Query("SELECT * FROM SearchEntity WHERE value = :value")
    abstract fun selectSearchByValue(value: String): SearchEntity?

    @Transaction
    @Query("SELECT * FROM PhotoEntity WHERE searchId = :id")
    abstract fun selectPhotosBySearchId(id: Int): List<PhotoEntity>
}