package com.warner.flickr.app.utilities

object Constants {
    const val BASE_URL = "https://www.flickr.com/"
    const val BASE_IMG_URL = "https://live.staticflickr.com/{server-id}/{id}_{secret}_{format}.jpg"

    const val SERVER_ID_SUFFIX = "{server-id}"
    const val ID_SUFFIX = "{id}"
    const val SECRET_SUFFIX = "{secret}"
    const val FORMAT_SUFFIX = "{format}"

    const val JSON_FORMAT = "json"
    const val PER_PAGE = 25
    const val NO_JSON_CALLBACK = 1
}