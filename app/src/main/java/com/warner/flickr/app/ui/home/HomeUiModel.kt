package com.warner.flickr.app.ui.home

import com.warner.flickr.app.repository.model.PhotoEntity
import com.warner.flickr.app.repository.service.HomeWebService
import com.warner.flickr.app.utilities.UrlUtils

data class HomeUiModel(
    val photos: List<PhotoUiModel>
) {

    companion object {

        fun fromEntity(photos: List<PhotoEntity>): HomeUiModel {
            val uiModelPhotos = arrayListOf<PhotoUiModel>()
            photos.forEach { photoEntity ->
                uiModelPhotos.add(
                    PhotoUiModel(
                        id = photoEntity.id,
                        title = photoEntity.title,
                        thumb = photoEntity.thumb,
                        image = photoEntity.image
                    )
                )
            }

            return HomeUiModel(photos = uiModelPhotos)
        }

        fun fromResponse(response: HomeWebService.PhotosResponse): HomeUiModel {
            val uiModelPhotos = arrayListOf<PhotoUiModel>()
            response.photos.photo.forEach { photoResponse ->
                val thumbnailUrl =
                    UrlUtils.toThumbnailUrl(
                        photoResponse.server, photoResponse.id, photoResponse.secret
                    )

                val imageUrl =
                    UrlUtils.toImageUrl(
                        photoResponse.server, photoResponse.id, photoResponse.secret
                    )

                uiModelPhotos.add(
                    PhotoUiModel(
                        id = photoResponse.id,
                        title = photoResponse.title,
                        thumb = thumbnailUrl,
                        image = imageUrl
                    )
                )
            }

            return HomeUiModel(photos = uiModelPhotos)
        }
    }
}

data class PhotoUiModel(
    val id: String,
    val title: String,
    val thumb: String,
    val image: String
)