package com.warner.flickr.app.di

import android.content.Context
import androidx.room.Room
import com.warner.flickr.app.repository.model.SearchDao
import com.warner.flickr.app.repository.model.SearchDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataBaseModule {

    @Singleton
    @Provides
    fun provideDataBase(@ApplicationContext context: Context) =
        Room.databaseBuilder(
            context,
            SearchDatabase::class.java,
            SearchDatabase.DATA_BASE_NAME
        ).build()

    @Singleton
    @Provides
    fun provideSearchDao(searchDatabase: SearchDatabase): SearchDao = searchDatabase.searchDao()
}