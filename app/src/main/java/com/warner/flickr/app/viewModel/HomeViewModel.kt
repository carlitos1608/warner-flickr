package com.warner.flickr.app.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.warner.flickr.app.repository.HomeRepository
import com.warner.flickr.app.repository.Resource
import com.warner.flickr.app.ui.home.HomeUiModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val homeRepository: HomeRepository
) : ViewModel() {

    private val _homeUiState = MutableLiveData<HomeUiState>()
    val homeUiState: LiveData<HomeUiState> = _homeUiState

    private var currentUiState = HomeUiState.idle()

    fun searchPhotos(searchText: String?) = runBlocking {
        viewModelScope.launch(Dispatchers.IO) {

            if (currentUiState.state == HomeUiState.LOADING) {
                // ignore when in process of loading
                return@launch
            }

            if (searchText.isNullOrEmpty()) {
                currentUiState = currentUiState.copy(state = HomeUiState.ERROR, errorMessage = "Please introduce input")
                _homeUiState.postValue(currentUiState)
                return@launch
            }

            // posting Loading value
            currentUiState = currentUiState.copy(state = HomeUiState.LOADING)
            _homeUiState.postValue(currentUiState)

            val resource = homeRepository.searchPhotos(searchText.lowercase())
            if (resource is Resource.Success) {
                resource.data?.let { uiModel ->
                    currentUiState = currentUiState.copy(
                        state = HomeUiState.SUCCESS,
                        homeUiModel = uiModel
                    )
                }
            }
            else {
                currentUiState = currentUiState.copy(
                    state = HomeUiState.ERROR,
                    errorMessage = resource.message
                )
            }

            _homeUiState.postValue(currentUiState)
        }
    }

    data class HomeUiState (
        val state: Int = UNKNOWN,
        val homeUiModel: HomeUiModel? = null,
        val errorMessage: String? = null,
    ) {

        companion object {

            // UI States
            const val UNKNOWN = -1
            const val LOADING = 0
            const val SUCCESS = 1
            const val ERROR = 2

            fun idle() = HomeUiState()
        }
    }
}