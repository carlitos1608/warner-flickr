package com.warner.flickr.app.repository

import android.util.Log
import com.warner.flickr.app.BuildConfig
import com.warner.flickr.app.repository.model.PhotoEntity
import com.warner.flickr.app.repository.model.SearchDao
import com.warner.flickr.app.repository.model.SearchEntity
import com.warner.flickr.app.repository.service.HomeWebService
import com.warner.flickr.app.ui.home.HomeUiModel
import com.warner.flickr.app.utilities.Constants
import com.warner.flickr.app.utilities.InternetChecker
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class HomeRepository @Inject constructor(
    private val internetChecker: InternetChecker,
    private val searchDao: SearchDao,
    private val homeWebService: HomeWebService
) {

    suspend fun searchPhotos(searchText: String): Resource<HomeUiModel> {
        Log.d(TAG, "executing searchPhotos by: $searchText")
        return try {
            val entity = searchDao.selectSearchByValue(searchText)
            Log.d(TAG, "database search entity: $entity")
            if (entity != null) {
                Log.d(TAG, "database search entity found, checking photos for entity")

                val photos = searchDao.selectPhotosBySearchId(entity.id)
                Log.d(TAG, "database photos entity: $photos")

                if (photos.isNotEmpty()) {
                    Log.d(TAG, "database photos entity found, returning response to populate UI")
                    return Resource.Success(HomeUiModel.fromEntity(photos))
                }
            }

            if (!internetChecker.isConnected()) {
                return Resource.Error("Error: No internet available, check your device and try again")
            }

            Log.d(TAG, "executing network call to fetch photos")
            val photosResponse = homeWebService.searchPhotos(BuildConfig.API_KEY, searchText, Constants.PER_PAGE, Constants.JSON_FORMAT, Constants.NO_JSON_CALLBACK)
            return photosResponse?.let { response ->

                Log.d(TAG, "response from cloud photosResponse: $response")

                val searchEntity = SearchEntity.fromResponse(searchText)
                val searchId = searchDao.insertSearch(searchEntity)

                Log.d(TAG, "inserting searchEntity: $searchEntity, searchEntityId: [$searchId] in database")

                val listOfPhotosBySearch = PhotoEntity.fromResponse(searchId, response)
                Log.d(TAG, "inserting photos by search criteria: $listOfPhotosBySearch")
                searchDao.insertAll(listOfPhotosBySearch)

                Log.d(TAG, "returning response to populate UI")
                Resource.Success(HomeUiModel.fromResponse(response))
            } ?: Resource.Error("Error fetching from Cloud; Null response retrieved")
        }
        catch (e: IOException) {
            // mostly internet exceptions
            Resource.Error("Error code: ${e.message}, status: ${e.cause?.message}", null)
        } catch (e: HttpException) {
            // mostly unauthorized keys
            Resource.Error("Error code: ${e.message}, status: ${e.cause?.message}", null)
        }
    }

    companion object {
        private const val TAG = "HomeRepository"
    }
}