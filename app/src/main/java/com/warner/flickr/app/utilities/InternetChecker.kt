package com.warner.flickr.app.utilities

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class InternetChecker @Inject constructor(@ApplicationContext private val context: Context) {

    fun isConnected(): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            ?: return false

        val cap = cm.getNetworkCapabilities(cm.activeNetwork)
            ?: return false

        return cap.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
    }
}