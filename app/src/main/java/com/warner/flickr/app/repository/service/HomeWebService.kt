package com.warner.flickr.app.repository.service

import retrofit2.http.GET
import retrofit2.http.Query

interface HomeWebService {

    @GET("services/rest/?method=flickr.photos.search")
    suspend fun searchPhotos(
        @Query("api_key") apiKey: String,
        @Query("text") text: String,
        @Query("per_page") perPage: Int,
        @Query("format") format: String,
        @Query("nojsoncallback") noJsonCallback: Int
    ): PhotosResponse?

    data class PhotosResponse(
        val photos: Photos,
        val stat: String
    )

    data class Photos(
        val page: Int,
        val photo: List<Photo>
    )

    data class Photo(
        val id: String,
        val owner: String,
        val secret: String,
        val server: String,
        val title: String
    )
}