package com.warner.flickr.app.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.warner.flickr.app.R
import com.warner.flickr.app.databinding.ItemPhotoBinding

class HomeAdapter(private val listener: OnClickListener): RecyclerView.Adapter<PhotoItemViewHolder>() {

    private var dataSource: List<PhotoUiModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PhotoItemViewHolder(
            ItemPhotoBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    override fun onBindViewHolder(holder: PhotoItemViewHolder, position: Int) {
        val uiModel = dataSource[position]
        holder.bind(uiModel, listener)
    }

    override fun getItemCount() = dataSource.size

    fun setData(data: List<PhotoUiModel>) {
        dataSource = data
        notifyDataSetChanged()
    }
}

/**
 * Holds the view for the Photo item.
 */
class PhotoItemViewHolder(private val binding: ItemPhotoBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(uiModel: PhotoUiModel, listener: OnClickListener) {
        binding.root.setOnClickListener {
            listener.onClicked(uiModel.image)
        }

        Picasso
            .with(binding.root.context)
            .load(uiModel.thumb)
            .placeholder(R.mipmap.ic_launcher)
            .fit()
            .into(binding.imgThumbnail)

        binding.txtId.text = uiModel.id
        binding.txtTitle.text = uiModel.title
    }
}

/**
 * Listener callback to inform image selected.
 */
interface OnClickListener {
    fun onClicked(imageUrl: String)
}