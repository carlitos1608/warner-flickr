package com.warner.flickr.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp(Application::class)
class FlickrApplication : Hilt_FlickrApplication()